package usm.aseantranslator.adapter;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import usm.aseantranslator.R;
import usm.aseantranslator.helper.BodyStringHandler;
import usm.aseantranslator.helper.LanguageDetect;
import usm.aseantranslator.model.Message;

/**
 * Created by addam01 on 12/04/2016.
 */
public class ChatListAdapter extends ArrayAdapter<Message> implements TextToSpeech.OnInitListener {
    private String mUserId;
    private String text = "Voice Unrecognized";
    private static TextToSpeech tts;
    LanguageDetect languageDetect = new LanguageDetect();
    private String language = "en";

    public ChatListAdapter(Context context, String userId, List<Message> messages) {
        super(context, 0, messages);
        tts = new TextToSpeech(getContext(),this);
        this.mUserId = userId;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).
                    inflate(R.layout.chat_item, parent, false);
            final ViewHolder holder = new ViewHolder();
//            holder.imageOther = (ImageView)convertView.findViewById(R.id.ivProfileOther);
            holder.ButtonLeft = (ImageButton) convertView.findViewById(R.id.left_record_button);
//            holder.imageMe = (ImageView)convertView.findViewById(R.id.ivProfileMe);
            holder.ButtonRight = (ImageButton) convertView.findViewById(R.id.right_record_button);
            holder.body = (EditText) convertView.findViewById(R.id.tvBody);
            holder.tTranslatedBody = (TextView) convertView.findViewById(R.id.tvTranslated);
//            holder.loading = (ProgressBar) convertView.findViewById(R.id.loadingBar);

            holder.ButtonLeft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    holder.loading.setVisibility(View.VISIBLE);
                    text = LanguageDetect.onCLickBody(holder.body.getText().toString()) ;
                    language = languageDetect.detectLanguage(text);
                    Toast.makeText(getContext(), language , Toast.LENGTH_LONG).show();
                    onInit(0);
                    speakOut();
//                    holder.loading.setVisibility(View.GONE);
                }
            });

            holder.ButtonRight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    holder.loading.setVisibility(View.VISIBLE);
                    text = LanguageDetect.onCLickBody(holder.body.getText().toString()) ;
                    language = languageDetect.detectLanguage(text);
                    Toast.makeText(getContext(), language , Toast.LENGTH_LONG).show();
                    onInit(0);
                    speakOut();
//                    holder.loading.setVisibility(View.GONE);
                }
            });

            convertView.setTag(holder);
        }

        final Message message = getItem(position);
        final ViewHolder holder = (ViewHolder)convertView.getTag();
        final boolean isMe = message.getUserId().equals(mUserId);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.END;
        LinearLayout.LayoutParams paramsStart = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsStart.gravity = Gravity.START;
        // Show-hide image based on the logged-in user.
        // Display the profile image to the right for our user, left for other users.
        if (isMe) {
//            holder.imageMe.setVisibility(View.VISIBLE);
//            holder.imageOther.setVisibility(View.GONE);
            holder.ButtonRight.setVisibility(View.VISIBLE);
            holder.ButtonLeft.setVisibility(View.GONE);
            holder.body.setGravity(Gravity.CENTER_VERTICAL | Gravity.END);
            holder.body.setLayoutParams(params);
            holder.body.setBackgroundResource(R.drawable.chat_right_body);
            holder.tTranslatedBody.setGravity(Gravity.CENTER_VERTICAL | Gravity.END);
            holder.tTranslatedBody.setLayoutParams(params);
            holder.tTranslatedBody.isTextSelectable();
            holder.tTranslatedBody.setSelectAllOnFocus(true);
            holder.tTranslatedBody.setBackgroundResource(R.drawable.chat_right_translate);
        } else {
//            holder.imageOther.setVisibility(View.VISIBLE);
//            holder.imageMe.setVisibility(View.GONE);
            holder.ButtonRight.setVisibility(View.GONE);
            holder.ButtonLeft.setVisibility(View.VISIBLE);
            holder.body.setGravity(Gravity.CENTER_VERTICAL | Gravity.START);
            holder.body.setLayoutParams(paramsStart);
            holder.body.setBackgroundResource(R.drawable.chat_left_body);
            holder.tTranslatedBody.setGravity(Gravity.CENTER_VERTICAL | Gravity.START);
            holder.tTranslatedBody.setLayoutParams(paramsStart);
            holder.tTranslatedBody.isTextSelectable();
            holder.tTranslatedBody.setSelectAllOnFocus(true);
            holder.tTranslatedBody.setBackgroundResource(R.drawable.chat_left_translate);
        }

//        final ImageView profileView = isMe ? holder.imageMe : holder.imageOther;
//        Picasso.with(getContext()).load(getProfileUrl(message.getUserId())).into(profileView);
        holder.body.setText(BodyStringHandler.getTranslatedBody(message.getBody()));
        holder.tTranslatedBody.setText(BodyStringHandler.getOriginalBody(message.getBody()));

        return convertView;
    }

    // Create a gravatar image based on the hash value obtained from userId
    private static String getProfileUrl(final String userId) {
        String hex = "";
        try {
            final MessageDigest digest = MessageDigest.getInstance("MD5");
            final byte[] hash = digest.digest(userId.getBytes());
            final BigInteger bigInt = new BigInteger(hash);
            hex = bigInt.abs().toString(16);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "http://www.gravatar.com/avatar/" + hex + "?d=identicon";
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {

//            int result = tts.setLanguage(new Locale("en"));
            int result = tts.setLanguage(new Locale(language));

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
                text = "Voice Unrecognized";
                speakOut();
            } else {
                Log.e("TTS", "Re initiate voice");
                Log.e("TTS", Arrays.toString(Locale.getAvailableLocales()));
                result = tts.setLanguage(new Locale(language));
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        }
    }

    private void speakOut() {
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }


    final class ViewHolder {
//        public ImageView imageOther;
//        public ImageView imageMe;
        public EditText body;
        public TextView tTranslatedBody;
        public ImageButton ButtonLeft;
        public ImageButton ButtonRight;
//        public ProgressBar loading;
    }

    public static void clearTTS(){
        tts.shutdown();
    }


}
