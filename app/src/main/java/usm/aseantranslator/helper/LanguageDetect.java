package usm.aseantranslator.helper;

import android.os.AsyncTask;
import android.util.Log;

import java.util.concurrent.ExecutionException;

import usm.aseantranslator.rest.DetectLanguageAPI;

/**
 * Created by Rounin-Rave on 08-May-16.
 */
public class LanguageDetect {
    static String LOG = LanguageDetect.class.getSimpleName();
    public DetectLanguageAPI detectLanguageAPI;

    public static String onCLickBody(String in ){
        String [] parts = in.split("\\n");
        Log.d(LOG , parts[0]);
        return parts[0];
    }

    public String detectLanguage(String in){
        in = onCLickBody(in);
        detect d = new detect();

        try {
            String out = d.execute(in).get();
            Log.d(LOG, out + " After executed");
            return  out;
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return "Error";
    }


    private class detect extends AsyncTask <String, Void, String>{

        @Override
        protected String doInBackground(String... params) {
            detectLanguageAPI  = new DetectLanguageAPI ("AIzaSyASm2nwujSEWyf6jGKbHYl81bQ2q2PS-X0");

            return detectLanguageAPI.detect(params[0]);
        }
        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
        }
    }

}
