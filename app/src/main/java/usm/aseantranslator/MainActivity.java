package usm.aseantranslator;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity{

    MediaPlayer mp;

    @Bind(R.id.btTooltipLeft)
    Button toolLeft;

    @Bind(R.id.btTooltipRight)
    Button toolRight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        mp = MediaPlayer.create(this, R.raw.sound_effect_click);



    }
    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @OnClick(R.id.message)
    public void onClickVoiceEvent(){
//        Toast.makeText(this, "Voice Event Chat", Toast.LENGTH_LONG).show();
        mp.setVolume(10,10);
        mp.start();
        Intent intent = new Intent(this, ChatActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.voice)
    public void onClickMessageEvent(){
//        Toast.makeText(this, "Message Event Chat", Toast.LENGTH_LONG).show();
        mp.start();
        Intent intent = new Intent(this, VoiceActivity.class);
        startActivity(intent);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }

    @OnClick(R.id.btTooltipLeft)
    void btLeftClick(){
        it.sephiroth.android.library.tooltip.Tooltip.make(this,
                new it.sephiroth.android.library.tooltip.Tooltip.Builder(101)
                        .anchor(toolLeft, it.sephiroth.android.library.tooltip.Tooltip.Gravity.TOP)
                        .closePolicy(new it.sephiroth.android.library.tooltip.Tooltip.ClosePolicy()
                                .insidePolicy(true, false)
                                .outsidePolicy(true, false), 4000)
                        .activateDelay(900)
                        .showDelay(400)
                        .withStyleId(R.style.CustomToolTip)
                        .text("Select for Keyboard Translation")
                        .maxWidth(600)
                        .withArrow(true)
                        .withOverlay(true).build()
        ).show();
    }

    @OnClick(R.id.btTooltipRight)
    void btRightClick(){
        it.sephiroth.android.library.tooltip.Tooltip.make(this,
                new it.sephiroth.android.library.tooltip.Tooltip.Builder(101)
                        .anchor(toolRight, it.sephiroth.android.library.tooltip.Tooltip.Gravity.TOP)
                        .closePolicy(new it.sephiroth.android.library.tooltip.Tooltip.ClosePolicy()
                                .insidePolicy(true, false)
                                .outsidePolicy(true, false), 4000)
                        .activateDelay(900)
                        .showDelay(400)
                        .withStyleId(R.style.CustomToolTip)
                        .text("Select for Voice Translation")
                        .maxWidth(600)
                        .withArrow(true)
                        .withOverlay(true).build()
        ).show();
    }
}
