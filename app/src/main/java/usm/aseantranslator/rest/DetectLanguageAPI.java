package usm.aseantranslator.rest;

import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by addam01 on 28/04/2016.
 */
public class DetectLanguageAPI {
    private String key;
    private String LOG = DetectLanguageAPI.class.getSimpleName();

    public DetectLanguageAPI(String apiKey){
        key = apiKey;
    }

    public String detect(String text) {
        StringBuilder result = new StringBuilder();
        try{
            String encodedText = URLEncoder.encode(text, "UTF-8");
            String urlStr =
                    "https://www.googleapis.com/language/translate/v2/detect?key=" + key + "&q=" + encodedText;

            URL url = new URL(urlStr);
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

            InputStream stream;
            if(conn.getResponseCode() == 200) { //Success
                stream = conn.getInputStream();
            }else{
                stream = conn.getErrorStream();
            }
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
            String line;
            while((line = reader.readLine())!=null){
                result.append(line);
            }
            JsonParser parser = new JsonParser();
            JsonElement element = parser.parse(result.toString());

            if(element.isJsonObject()){
                JsonObject obj = element.getAsJsonObject();
                if(obj.get("error")==null){
                    return obj.get("data").getAsJsonObject()
                            .get("detections").getAsJsonArray()
                            .get(0).getAsJsonArray()
                            .get(0).getAsJsonObject()
                            .get("language").getAsString();
                }
            }
            if (conn.getResponseCode() != 200){
                Log.e(LOG, String.valueOf(result));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}

