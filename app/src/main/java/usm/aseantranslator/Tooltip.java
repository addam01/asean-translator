package usm.aseantranslator;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.parse.Parse;
import com.parse.ParseObject;
import com.parse.interceptors.ParseLogInterceptor;

import butterknife.ButterKnife;
import butterknife.OnClick;
import usm.aseantranslator.model.Message;

public class Tooltip extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tooltip);

        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Register your parse models here
        ParseObject.registerSubclass(Message.class);

        // set applicationId and server based on the values in the Heroku settings.
        // any network interceptors must be added with the Configuration Builder given this syntax
        try{
            Parse.initialize(new Parse.Configuration.Builder(this)
                    .applicationId("qsean") // should correspond to APP_ID env variable
                    .clientKey("myMasterKey")
                    .addNetworkInterceptor(new ParseLogInterceptor())
                    .server("https://radiant-hamlet-65467.herokuapp.com/parse/").build());
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    @OnClick(R.id.btContinue)
    void btContinueOnClick(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

}
