package usm.aseantranslator;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.ParseAnonymousUtils;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gcardone.junidecode.Junidecode;
import usm.aseantranslator.adapter.ChatListAdapter;
import usm.aseantranslator.model.Message;
import usm.aseantranslator.rest.TranslateAPI;

public class ChatActivity extends AppCompatActivity {
    static final String LOG = ChatActivity.class.getSimpleName();
    static final int MAX_CHAT_MESSAGES_TO_SHOW = 50;
    TranslateAPI translateAPI;
    SharedPreferences sharedPreferences;
    ParseObject ParseMessage;
    String language = "ms";


    static final String USER_ID_KEY = "userId";
    static final String BODY_KEY = "body";

    @Bind(R.id.etMessage)
    public EditText message;

    @Bind(R.id.btSend)
    ImageButton send;

    @Bind(R.id.lvChat)
    ListView lvChat;

    @Bind(R.id.btTooltipLeft)
    Button toolLeft;

    @Bind(R.id.btTooltipRight)
    Button toolRight;

    @Bind(R.id.loadingBar)
    ProgressBar loading;
    ArrayList<Message> mMessages;
    ChatListAdapter mAdapter;
    // Keep track of initial load to scroll to the bottom of the ListView
    boolean mFirstLoad;

    TranslateREST translateREST;

    // Create a handler which can run code periodically
    static final int POLL_INTERVAL = 60000; // milliseconds
    Handler mHandler = new Handler();  // android.os.Handler
    Runnable mRefreshMessagesRunnable = new Runnable() {
        @Override
        public void run() {
            refreshMessage();
            mHandler.postDelayed(this, POLL_INTERVAL);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        if(Build.VERSION.SDK_INT > 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        loading.setVisibility(View.GONE);
        sharedPreferences = getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        language = sharedPreferences.getString("Language", "ms");
        // User login
        if (ParseUser.getCurrentUser() != null) { // start with existing user
            startWithCurrentUser();
        } else { // If not logged in, login as a new anonymous user
            login();
        }
        refreshMessage();
        mHandler.postDelayed(mRefreshMessagesRunnable, POLL_INTERVAL);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void startWithCurrentUser() {
        setupMessagePosting();
    }

    private void setupMessagePosting() {
        mMessages = new ArrayList<>();
        // Automatically scroll to the bottom when a data set change notification is received and only if the last item is already visible on screen. Don't scroll to the bottom otherwise.
        lvChat.setTranscriptMode(1);
        mFirstLoad = true;

        final String userId = ParseUser.getCurrentUser().getObjectId();
        mAdapter = new ChatListAdapter(ChatActivity.this, userId, mMessages);
        lvChat.setAdapter(mAdapter);
        lvChat.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(ChatActivity.this, "Item clicked at "+position, Toast.LENGTH_LONG).show();
            }
        });

    }

    @OnClick(R.id.btSend)
    void btSendOnClick(View v){


        PopupMenu popupMenu = new PopupMenu(ChatActivity.this, v);
        try{
            Field[] fields = popupMenu.getClass().getDeclaredFields();
            for(Field field : fields){
                if("mPopup".equals(field.getName())){
                    field.setAccessible(true);
                    Object menuPopupHandler = field.get(popupMenu);
                    Class<?> classPopopHandler = Class.forName(menuPopupHandler.getClass().getName());
                    Method setForceIcons = classPopopHandler.getMethod("setForceShowIcon", boolean.class);
                    setForceIcons.invoke(menuPopupHandler,true);
                    break;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        popupMenu.inflate(R.menu.activity_main2_drawer);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();

                switch (id){
                    case R.id.en:
                        language =  "en";
                        send.setImageResource(R.drawable.english_keyboard);doSend();
                        break;
                    case R.id.ms:
                        language =  "ms";
                        send.setImageResource(R.drawable.malay_keyboard);doSend();
                        break;
                    case R.id.th:
                        language =  "th";
                        send.setImageResource(R.drawable.thai_keyboard_right);doSend();
                        break;
                    case R.id.tl:
                        language =  "tl";doSend();break;
                    case R.id.id:
                        language =  "id";doSend();break;
                    case R.id.km:
                        language =  "km";doSend();break;
                    case R.id.th1:
                        language =  "th";doSend();break;
                    case R.id.ms1:
                        language =  "ms";doSend();break;
                    case R.id.en1:
                        language =  "en";doSend();break;
                    case R.id.my:
                        language =  "my";doSend();break;
                }
                Toast.makeText(ChatActivity.this
                        , "Language is set to translate to: " + language
                        ,Toast.LENGTH_LONG).show();
                return false;
            }
        });
        popupMenu.show();
    }

    private void doSend() {
        String data = message.getText().toString();
        if(!data.isEmpty()){
            loading.setVisibility(View.VISIBLE);
            translateREST = new TranslateREST();
            translateREST.execute(data);
            refreshMessage();

        }else refreshMessage();
    }


    private void refreshMessage() {
        //TODO something
        // Construct query to execute
        ParseQuery<Message> query = ParseQuery.getQuery(Message.class);
        // Configure limit and sort order
        query.setLimit(MAX_CHAT_MESSAGES_TO_SHOW);
        query.orderByAscending("createdAt");
        // Execute query to fetch all messages from Parse asynchronously
        // This is equivalent to a SELECT query with SQL
        query.findInBackground(new FindCallback<Message>() {
            public void done(List<Message> messages, ParseException e) {
                if (e == null) {
                    mMessages.clear();
                    mMessages.addAll(messages);
                    mAdapter.notifyDataSetChanged(); // update adapter
                    // Scroll to the bottom of the list on initial load
                    if (mFirstLoad) {
                        lvChat.setSelection(mAdapter.getCount() - 1);
                        mFirstLoad = false;
                    }
                } else {
                    Log.e("message", "Error Loading Messages" + e);
                }
            }
        });
    }


    void login(){
        ParseAnonymousUtils.logIn(new LogInCallback() {
            @Override
            public void done(ParseUser user, ParseException e) {
                if (e != null) {
                    Log.e(LOG, "Anonymous login failed: ", e);
                } else {
                    startWithCurrentUser();
                }
            }
        });
    }

    @Override
    protected void onStop() {
//        translateREST.cancel(true);
        ChatListAdapter.clearTTS();
        finish();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
//        translateREST.cancel(true);
        ChatListAdapter.clearTTS();
        finish();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
//        translateREST.cancel(true);
        finish();
        super.onBackPressed();
    }

    private class TranslateREST extends AsyncTask<String, Void, String> {

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected String doInBackground(String... params) {
            if (isCancelled())
            {
                return null;
            }
            else{
                try{
                    translateAPI = new TranslateAPI("AIzaSyASm2nwujSEWyf6jGKbHYl81bQ2q2PS-X0");
                    Thread.sleep(5000);
                    translated(params[0]);
                    return params[0];

                }catch (Exception e){
                    Log.e(LOG,e.getMessage());
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            translated(result);
            message.setText(null);
            ParseMessage.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    Toast.makeText(ChatActivity.this, "Successfully created message on Parse",
                            Toast.LENGTH_SHORT).show();
//                    loading.setVisibility(View.GONE);
//                    refreshMessage();
                }
            });
            loading.setVisibility(View.GONE);
            refreshMessage();
        }

        public void translated (String data){
            String body = translateAPI.translate(
                    data,
                    "en",language);
            ParseMessage = ParseObject.create("Message");
            ParseMessage.put(Message.USER_ID_KEY, ParseUser.getCurrentUser().getObjectId());
            ParseMessage.put(Message.BODY_KEY, data+"|"+body+"+"+ Junidecode.unidecode(body));

        }
    }

    @OnClick(R.id.btTooltipLeft)
    void btLeftClick(){
        it.sephiroth.android.library.tooltip.Tooltip.make(this,
                new it.sephiroth.android.library.tooltip.Tooltip.Builder(101)
                        .anchor(toolLeft, it.sephiroth.android.library.tooltip.Tooltip.Gravity.TOP)
                        .closePolicy(new it.sephiroth.android.library.tooltip.Tooltip.ClosePolicy()
                                .insidePolicy(true, false)
                                .outsidePolicy(true, false), 4000)
                        .activateDelay(900)
                        .showDelay(400)
                        .withStyleId(R.style.CustomToolTip)
                        .text("Enter your desire text for translation. Then tap the keyboard")
                        .maxWidth(600)
                        .withArrow(true)
                        .withOverlay(true).build()
        ).show();
    }

    @OnClick(R.id.btTooltipRight)
    void btRightClick(){
        it.sephiroth.android.library.tooltip.Tooltip.make(this,
                new it.sephiroth.android.library.tooltip.Tooltip.Builder(101)
                        .anchor(toolRight, it.sephiroth.android.library.tooltip.Tooltip.Gravity.TOP)
                        .closePolicy(new it.sephiroth.android.library.tooltip.Tooltip.ClosePolicy()
                                .insidePolicy(true, false)
                                .outsidePolicy(true, false), 4000)
                        .activateDelay(900)
                        .showDelay(400)
                        .withStyleId(R.style.CustomToolTip)
                        .text("Select the translation language and we'll translate")
                        .maxWidth(600)
                        .withArrow(true)
                        .withOverlay(true).build()
        ).show();
    }
}
