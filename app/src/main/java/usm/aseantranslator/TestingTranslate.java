package usm.aseantranslator;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.api.services.translate.Translate;

import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gcardone.junidecode.Junidecode;
import usm.aseantranslator.helper.BodyStringHandler;
import usm.aseantranslator.rest.TranslateAPI;

public class TestingTranslate extends AppCompatActivity implements TextToSpeech.OnInitListener{
    String LOG = TestingTranslate.class.getSimpleName();
    TranslateAPI translateAPI;
    private TextToSpeech tts;

    @Bind(R.id.translatedText)
    TextView vTranslate;

    @Bind(R.id.translatedEditText)
    EditText eTranslate;

    @Bind(R.id.BTranslate)
    Button bTranslate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testing_translate);
        ButterKnife.bind(this);

        tts = new TextToSpeech(this, this);
        bTranslate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String in =  eTranslate.getText().toString();
                String text = "Hello+Sarah";
//                vTranslate.setText(Junidecode.unidecode(in));
                vTranslate.setText(BodyStringHandler.findRoman(text));
            }
        });


//        bTranslate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                speakOut();
//
//
//            }
//        });

    }

    private void speakOut() {
        String text = eTranslate.getText().toString();

        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }


    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(new Locale("th"));

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {
                bTranslate.setEnabled(true);
                speakOut();
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        }
    }
    @Override
    public void onDestroy() {
        // Don't forget to shutdown tts!
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }

        super.onDestroy();
    }
}
