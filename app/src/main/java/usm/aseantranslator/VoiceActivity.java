package usm.aseantranslator;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.ParseAnonymousUtils;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gcardone.junidecode.Junidecode;
import it.sephiroth.android.library.tooltip.Tooltip;
import usm.aseantranslator.adapter.ChatListAdapter;
import usm.aseantranslator.model.Message;
import usm.aseantranslator.rest.TranslateAPI;

public class VoiceActivity extends AppCompatActivity{

    TranslateAPI translateAPI;
    TranslateREST translateREST;
    ParseObject ParseMessage;
    SharedPreferences sharedPreferences;
    String language;
    String translated;

    static final String LOG = VoiceActivity.class.getSimpleName();
    static final int MAX_CHAT_MESSAGES_TO_SHOW = 50;

    static final String USER_ID_KEY = "userId";
    static final String BODY_KEY = "body";

    @Bind(R.id.loadingBar)
    ProgressBar loading;

//    @Bind(R.id.btSend)
    ImageButton send;
//    @Bind(R.id.btSendForeign)
    ImageButton sendForeign;
    @Bind(R.id.btTooltipLeft)
    Button toolLeft;
    @Bind(R.id.btTooltipMiddle)
    Button toolMiddle;
    @Bind(R.id.btTooltipRight)
    Button toolRight;


    @Bind(R.id.lvChat)
    ListView lvChat;

    ArrayList<Message> mMessages;
    ChatListAdapter mAdapter;
    // Keep track of initial load to scroll to the bottom of the ListView
    boolean mFirstLoad;

    // Create a handler which can run code periodically
    static final int POLL_INTERVAL = 60000; // milliseconds
    Handler mHandler = new Handler();  // android.os.Handler
    Runnable mRefreshMessagesRunnable = new Runnable() {
        @Override
        public void run() {
            refreshMessage();
            mHandler.postDelayed(this, POLL_INTERVAL);
        }
    };

    private void refreshMessage() {
        //TODO something
        // Construct query to execute
        ParseQuery<Message> query = ParseQuery.getQuery(Message.class);
        // Configure limit and sort order
        query.setLimit(MAX_CHAT_MESSAGES_TO_SHOW);
        query.orderByAscending("createdAt");
        // Execute query to fetch all messages from Parse asynchronously
        // This is equivalent to a SELECT query with SQL
        query.findInBackground(new FindCallback<Message>() {
            public void done(List<Message> messages, ParseException e) {
                if (e == null) {
                    mMessages.clear();
                    mMessages.addAll(messages);
                    mAdapter.notifyDataSetChanged(); // update adapter
                    // Scroll to the bottom of the list on initial load
                    if (mFirstLoad) {
                        lvChat.setSelection(mAdapter.getCount() - 1);
                        mFirstLoad = false;
                    }
                } else {
                    Log.e("message", "Error Loading Messages" + e);
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice);

        if(Build.VERSION.SDK_INT > 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        ButterKnife.bind(this);
        loading.setVisibility(View.GONE);
        sharedPreferences = getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        language = sharedPreferences.getString("Language", "en");
        translated = "en";
//        TODO Take the shared preference Language and apply it in the Translate language
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        send = (ImageButton) findViewById(R.id.btSend);
        sendForeign = (ImageButton) findViewById(R.id.btSendForeign);

//        AIConfiguration config = new AIConfiguration("9ad8c0ecfaa248d19e55a003307c4828",
//                AIConfiguration.SupportedLanguages.English,
//                AIConfiguration.RecognitionEngine.System);
//
//        AIService aiservice = AIService.getService(this, config);
//        aiservice.setListener(this);
        // User login
        if (ParseUser.getCurrentUser() != null) { // start with existing user
            startWithCurrentUser();
        } else { // If not logged in, login as a new anonymous user
            login();
        }
        refreshMessage();
        mHandler.postDelayed(mRefreshMessagesRunnable, POLL_INTERVAL);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void login() {
        ParseAnonymousUtils.logIn(new LogInCallback() {
            @Override
            public void done(ParseUser user, ParseException e) {
                if (e != null) {
                    Log.e(LOG, "Anonymous login failed: ", e);
                } else {
                    startWithCurrentUser();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
//        translateREST.cancel(true);
        ChatListAdapter.clearTTS();
        finish();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
//        translateREST.cancel(true);
        ChatListAdapter.clearTTS();
        finish();
        super.onBackPressed();
    }

    @OnClick(R.id.btSend)
    protected void onSendClick(View view){

        PopupMenu popupMenu = new PopupMenu(VoiceActivity.this, view);
        try{
            Field[] fields = popupMenu.getClass().getDeclaredFields();
            for(Field field : fields){
                if("mPopup".equals(field.getName())){
                    field.setAccessible(true);
                    Object menuPopupHandler = field.get(popupMenu);
                    Class<?> classPopopHandler = Class.forName(menuPopupHandler.getClass().getName());
                    Method setForceIcons = classPopopHandler.getMethod("setForceShowIcon", boolean.class);
                    setForceIcons.invoke(menuPopupHandler,true);
                    break;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        popupMenu.inflate(R.menu.activity_main2_drawer);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();

                switch (id){
                    case R.id.en:
                        language =  "en";
                        send.setImageResource(R.drawable.en);break;
                    case R.id.ms:
                        language =  "ms";
                        send.setImageResource(R.drawable.my);break;
                    case R.id.th:
                        language =  "th";
                        send.setImageResource(R.drawable.th);break;
                    case R.id.tl:
                        language =  "tl";
                        send.setImageResource(R.drawable.tl);break;
                    case R.id.id:
                        language =  "id";
                        send.setImageResource(R.drawable.in);break;
                    case R.id.km:
                        language =  "km";
                        send.setImageResource(R.drawable.cm);break;
                    case R.id.th1:
                        language =  "th";
                        send.setImageResource(R.drawable.ls);break;
                    case R.id.ms1:
                        language =  "ms";
                        send.setImageResource(R.drawable.br);break;
                    case R.id.en1:
                        language =  "en";
                        send.setImageResource(R.drawable.sg);break;
                    case R.id.my:
                        language =  "my";
                        send.setImageResource(R.drawable.my1);break;
                }
                Toast.makeText(VoiceActivity.this
                        , "Language is set to translate from: " + language
                        ,Toast.LENGTH_LONG).show();


                return false;
            }
        });
        popupMenu.show();
    }

    @OnClick(R.id.btSendForeign)
    protected void onSendForeignClick(View view){
        PopupMenu popupMenu = new PopupMenu(VoiceActivity.this, view);
        try{
            Field[] fields = popupMenu.getClass().getDeclaredFields();
            for(Field field : fields){
                if("mPopup".equals(field.getName())){
                    field.setAccessible(true);
                    Object menuPopupHandler = field.get(popupMenu);
                    Class<?> classPopopHandler = Class.forName(menuPopupHandler.getClass().getName());
                    Method setForceIcons = classPopopHandler.getMethod("setForceShowIcon", boolean.class);
                    setForceIcons.invoke(menuPopupHandler,true);
                    break;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        popupMenu.inflate(R.menu.activity_main2_drawer);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();

                switch (id){
                    case R.id.en:
                        translated =  "en";
                        sendForeign.setImageResource(R.drawable.en);break;
                    case R.id.ms:
                        translated =  "ms";
                        sendForeign.setImageResource(R.drawable.my);break;
                    case R.id.th:
                        translated =  "th";
                        sendForeign.setImageResource(R.drawable.th);break;
                    case R.id.tl:
                        translated =  "tl";
                        sendForeign.setImageResource(R.drawable.tl);break;
                    case R.id.id:
                        translated =  "id";
                        sendForeign.setImageResource(R.drawable.in);break;
                    case R.id.km:
                        translated =  "km";
                        sendForeign.setImageResource(R.drawable.cm);break;
                    case R.id.th1:
                        translated =  "th";
                        sendForeign.setImageResource(R.drawable.ls);break;
                    case R.id.ms1:
                        translated =  "ms";
                        sendForeign.setImageResource(R.drawable.br);break;
                    case R.id.en1:
                        translated =  "en";
                        sendForeign.setImageResource(R.drawable.sg);break;
                    case R.id.my:
                        translated =  "my";
                        sendForeign.setImageResource(R.drawable.my1);break;
                }
                Toast.makeText(VoiceActivity.this
                        , "translated is set to translate to: " + translated
                        ,Toast.LENGTH_LONG).show();
                return true;
            }
        });
        popupMenu.show();
    }

    @OnClick(R.id.btRecord)
    public void onClickRecord(){
        loading.setVisibility(View.VISIBLE);
//        aiservice.startListening();
        promptVoiceInput();
    }
    
    private void promptVoiceInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, language);
        try {
            startActivityForResult(intent, 1);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),"I'm Sorry, your phone isn't supported by Google. Please try again",
                    Toast.LENGTH_SHORT).show();
        }
    }

//    @Override
//    public void onResult(AIResponse response) {
//        Result result = response.getResult();
//
//        // Get parameters
//        String parameterString = "";
//        if (result.getParameters() != null && !result.getParameters().isEmpty()) {
//            for (final Map.Entry<String, JsonElement> entry : result.getParameters().entrySet()) {
//                parameterString += "(" + entry.getKey() + ", " + entry.getValue() + ") ";
//            }
//        }
//
//        String data = result.getResolvedQuery();
//        translateREST  = new TranslateREST();
//        translateREST.execute(data);
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 1: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> text = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

//                    txtText.setText(text.get(0));
                    Log.d("Translation", text.get(0));
                    translateREST  = new TranslateREST();
                    translateREST.execute(text.get(0));
                }
                break;
            }

        }
    }

    private void startWithCurrentUser() {
        setupMessagePosting();
    }

    private void setupMessagePosting() {
        mMessages = new ArrayList<>();
        // Automatically scroll to the bottom when a data set change notification is received and only if the last item is already visible on screen. Don't scroll to the bottom otherwise.
        lvChat.setTranscriptMode(1);
        mFirstLoad = true;

        final String userId = ParseUser.getCurrentUser().getObjectId();
        mAdapter = new ChatListAdapter(VoiceActivity.this, userId, mMessages);
        lvChat.setAdapter(mAdapter);
    }


//    @Override
//    public void onError(AIError error) {
//        Toast.makeText(this, "An Error have occured", Toast.LENGTH_LONG).show();
//        loading.setVisibility(View.GONE);
//
//    }
//
//    @Override
//    public void onAudioLevel(float level) {
//
//    }
//
//    @Override
//    public void onListeningStarted() {
//
//    }

//    @Override
//    public void onListeningCanceled() {
//
//    }
//
//    @Override
//    public void onListeningFinished() {
//
//    }
    private class TranslateREST extends AsyncTask<String, Void, String> {

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected String doInBackground(String... params) {
            if (isCancelled()){
                return null;
            }
            else{
                try{
                    translateAPI = new TranslateAPI("AIzaSyASm2nwujSEWyf6jGKbHYl81bQ2q2PS-X0");
                    Thread.sleep(5000);
                    translated(params[0]);
                    return params[0];

                }catch (Exception e){
                    Log.e(LOG,e.getMessage());
                }
                return null;
            }

        }

        @Override
        protected void onPostExecute(String result) {
//            progressDialog.dismiss();

            super.onPostExecute(result);
            Log.d(LOG, result);
            ParseMessage.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    Toast.makeText(VoiceActivity.this, "Successfully created message on Parse",
                            Toast.LENGTH_SHORT).show();
                    loading.setVisibility(View.GONE);

                    refreshMessage();
                }
            });
        }

        public void translated (String data){
            String body = translateAPI.translate(
                    data,
                    language,translated);
            Log.d("Translate", translated);
            Log.d("Translate", language);
            Log.d("Translate", data);
            Log.d("Translate", body);
            ParseMessage = ParseObject.create("Message");
            ParseMessage.put(Message.USER_ID_KEY, ParseUser.getCurrentUser().getObjectId());
            ParseMessage.put(Message.BODY_KEY, data+"|"+body+"+"+ Junidecode.unidecode(body));

        }
    }

    @OnClick(R.id.btTooltipLeft)
    void TooltipLeftClick(){


        Tooltip.make(this,
                new Tooltip.Builder(101)
                        .anchor(toolLeft, Tooltip.Gravity.TOP)
                        .closePolicy(new Tooltip.ClosePolicy()
                                .insidePolicy(true, false)
                                .outsidePolicy(true, false), 4000)
                        .activateDelay(900)
                        .showDelay(400)
                        .withStyleId(R.style.CustomToolTip)
                        .text("Select the language you are speaking.")
                        .maxWidth(600)
                        .withArrow(true)
                        .withOverlay(true).build()
        ).show();
    }

    @OnClick(R.id.btTooltipMiddle)
    void TooltipMiddleClick(){


        Tooltip.make(this,
                new Tooltip.Builder(101)
                        .anchor(toolMiddle, Tooltip.Gravity.TOP)
                        .closePolicy(new Tooltip.ClosePolicy()
                                .insidePolicy(true, false)
                                .outsidePolicy(true, false), 4000)
                        .activateDelay(900)
                        .showDelay(400)
                        .withStyleId(R.style.CustomToolTip)
                        .text("Tap to translate")
                        .maxWidth(600)
                        .withArrow(true)
                        .withOverlay(true).build()
        ).show();
    }

    @OnClick(R.id.btTooltipRight)
    void TooltipRightClick(){


        Tooltip.make(this,
                new Tooltip.Builder(101)
                        .anchor(toolRight, Tooltip.Gravity.TOP)
                        .closePolicy(new Tooltip.ClosePolicy()
                                .insidePolicy(true, false)
                                .outsidePolicy(true, false), 4000)
                        .activateDelay(900)
                        .showDelay(400)
                        .withStyleId(R.style.CustomToolTip)
                        .text("Select language you are translating.")
                        .maxWidth(600)
                        .withArrow(true)
                        .withOverlay(true).build()
        ).show();
    }

}
