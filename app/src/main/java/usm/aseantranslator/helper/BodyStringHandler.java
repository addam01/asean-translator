package usm.aseantranslator.helper;

import android.util.Log;

/**
 * Created by addam01 on 19/04/2016.
 */
public class BodyStringHandler {
    static String LOG = BodyStringHandler.class.getSimpleName();

    public static String getOriginalBody (String in){
//        Log.d(LOG, in);
        String [] parts = in.split("\\|");
        Log.d(LOG , parts[0]);
        return parts[0];
    }

    public static String getTranslatedBody (String in){
//        Log.d(LOG, in);
        String [] parts = in.split("\\|");
        Log.d(LOG, parts[0]);
        return findRoman(parts[1]);
    }

    public static String findRoman (String in){
        Log.d("findroman", in);
        if(in.contains("+")){
            return in.replace("+", "\n");
        }else
        return in;
    }
}
